import 'package:flutter/material.dart';
import 'package:icall/secret_key.dart';
import 'package:zego_uikit_prebuilt_call/zego_uikit_prebuilt_call.dart';

class CallPage extends StatelessWidget {
  const CallPage({
    super.key,
    required this.callID,
    required this.userId,
    required this.username,
  });
  final String callID;
  final String userId;
  final String username;

  @override
  Widget build(BuildContext context) {
    return ZegoUIKitPrebuiltCall(
      appID: int.parse(appId ?? ""),
      appSign: appSign ?? "",
      userID: userId,
      userName: username,
      callID: callID,
      config: ZegoUIKitPrebuiltCallConfig.oneOnOneVideoCall(),
    );
  }
}
