import 'package:flutter_dotenv/flutter_dotenv.dart';

final appId = dotenv.env['APP_ID'];
final appSign = dotenv.env['APP_SIGN'];
